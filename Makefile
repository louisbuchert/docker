all: build run

build:
	docker build --build-arg UID=$(shell id -u) --build-arg GID=$(shell id -g) -t yocto-dunfell .

run:
	docker run -it \
		-v ${CURDIR}/..:${PWD}/..:shared \
		-w ${PWD}/.. \
		--entrypoint="/bin/bash" \
		yocto-dunfell \
		-c "/bin/bash"

